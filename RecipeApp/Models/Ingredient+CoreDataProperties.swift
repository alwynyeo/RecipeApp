//
//  Ingredient+CoreDataProperties.swift
//  RecipeApp
//
//  Created by Alwyn Yeo on 8/5/21.
//
//

import Foundation
import CoreData


extension Ingredient {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Ingredient> {
        return NSFetchRequest<Ingredient>(entityName: "Ingredient")
    }

    @NSManaged public var name: String?
    @NSManaged public var type: String?
    @NSManaged public var quantity: String?
    @NSManaged public var recipe: Recipe?

}

extension Ingredient : Identifiable {

}
