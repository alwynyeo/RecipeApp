//
//  NewRecipe.swift
//  RecipeApp
//
//  Created by Alwyn Yeo on 8/5/21.
//

// MARK: - Struct
struct NewRecipe {
    // MARK: - Declarations
    var title = ""
    var type = ""
    var imageUrl = ""
    var ingredients = [Ingredient]()
    var steps = [String]()

    struct Ingredient {
        var name = ""
        var type = ""
        var quantity = ""
    }
}
