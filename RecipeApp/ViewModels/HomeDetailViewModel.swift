//
//  HomeDetailViewModel.swift
//  RecipeApp
//
//  Created by Alwyn Yeo on 8/4/21.
//

// MARK: - Class
final class HomeDetailViewModel {

    // MARK: - Public Declarations
    var title: String {
        get { recipe.title }
    }

    var type: String {
        get { recipe.type }
    }

    var imageUrl: String {
        get { recipe.imageUrl }
    }

    private(set) lazy var ingredients: [Ingredient] = recipe.ingredient?.allObjects as! [Ingredient] // Read-only property

    var steps: [String] {
        get { recipe.steps }
    }

    enum Field { case title, type, imageUrl }

    // MARK: - Private Declarations
    private var recipe: Recipe

    // MARK: - Initializer
    init(recipe: Recipe) {
        self.recipe = recipe
    }

    // MARK: - Public Methods
    func update(for field: Field, with data: String) {
        switch field {
            case .title: recipe.title = data
            case .type: recipe.type = data
            case .imageUrl: recipe.imageUrl = data
        }
        update()
    }

    func appendIngredient(_ ingredient: NewRecipe.Ingredient) {
        DataService.shared.createIngredient(for: recipe, ingredient) { [weak self] status in
            switch status {
                case .success(let ingredient):
                    self?.ingredients.append(ingredient)
                case .failure(let error):
                    print("\(String(describing: error)) at \(#function) in \(#file)")
            }
        }
    }

    func updateIngredient(oldName: String, newData: NewRecipe.Ingredient) {
        DataService.shared.updateIngredient(from: oldName, to: newData) { success, error in
            guard error == nil && success else {
                print("\(String(describing: error)) at \(#function) in \(#file)")
                return
            }
        }
    }

    func appendStep(step: String) {
        recipe.steps.append(step)
        update()
    }
    
    func updateStep(oldStep: String, newStep: String) {
        for position in recipe.steps.indices {
            if recipe.steps[position] == oldStep {
                recipe.steps[position] = newStep
            }
        }
        update()
    }

    func removeIngredient(at index: Int) {
        DataService.shared.deleteIngredient(ingredients.remove(at: index)) { success, error in
            guard error == nil && success else {
                print("\(String(describing: error)) at \(#function) in \(#file)")
                return
            }
        }
    }

    func move(_ step: String, from: Int, to: Int) {
        recipe.steps.remove(at: from)
        recipe.steps.insert(step, at: to)
        update()
    }

    func removeStep(at index: Int) {
        recipe.steps.remove(at: index)
        update()
    }

    // MARK: - Private Methods
    private func update() {
        DataService.shared.updateRecipe() { success, error in
            guard error == nil && success else {
                print("\(String(describing: error)) at \(#function) in \(#file)")
                return
            }
        }
    }
}
