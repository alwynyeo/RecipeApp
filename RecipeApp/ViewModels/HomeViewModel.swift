//
//  HomeViewModel.swift
//  RecipeApp
//
//  Created by Alwyn Yeo on 8/3/21.
//

import Foundation
import SWXMLHash

// MARK: - Class
final class HomeViewModel {

    // MARK: - Public Declarations
    var selectedFilterType = "All"
    private(set) lazy var filteredRecipes: [Recipe] = [] // Read-only property

    // MARK: - Private Declarations
    private var recipes: [Recipe] = []

    // MARK: - Public Methods
    func fetchAllRecipes() {
        // Load and add XML data to core data if core data is empty
        if DataService.shared.isEmpty() {
            loadXMLData()
        }
        // Fetch all core data objects
        DataService.shared.fetchAllRecipes { [weak self] status in
            switch status {
                case .success(let recipes):
                    self?.recipes = recipes
                    self?.filteredRecipes = recipes
                case .failure(let error):
                    print("\(error) at \(#function) in \(#file)")
            }
        }
    }

    func deleteRecipe(_ recipe: Recipe, at index: Int) {
        DataService.shared.deleteRecipe(at: recipe) { [weak self] success, error in
            guard error == nil && success else {
                print("\(String(describing: error)) at \(#function) in \(#file)")
                return
            }
            guard let deletedFilteredRecipe = self?.filteredRecipes.remove(at: index) else { return }
            guard let deletedRecipeIndex = self?.recipes.firstIndex(of: deletedFilteredRecipe) else { return }
            self?.recipes.remove(at: deletedRecipeIndex)
        }
    }

    func deleteAllRecipes() {
        recipes.forEach {
            DataService.shared.deleteRecipe(at: $0) { success, error in
                guard error == nil && success else {
                    print("\(String(describing: error)) at \(#function) in \(#file)")
                    return
                }
            }
        }
    }

    func filterRecipes() {
        filteredRecipes = selectedFilterType != "All"
            ? recipes.filter { $0.type.contains(selectedFilterType) }
            : recipes
    }

    private func loadXMLData() {
        // Load data.xml path from bundle
        if let path = Bundle.main.url(forResource: "data", withExtension: "xml") {
            var allRecipes: [XMLIndexer] = []
            var ingredients: [NewRecipe.Ingredient] = []
            var steps: [String] = []

            do {
                // Save the contents in data.xml file to a string
                let contents = try String(contentsOf: path)
                let xml = SWXMLHash.parse(contents)
                // This is all the recipe objects
                allRecipes = xml["recipeset"]["recipe"].all
            } catch {
                print(error)
            }

            // Loop all recipe objects
            allRecipes.forEach {
                guard let title = $0["title"].element?.text else { return }
                guard let type = $0["recipetype"].element?.text else { return }
                guard let imageUrl = $0["imageURL"].element?.text else { return }
                let allIngredients = $0["ingredients"]["ingredient"].all
                let allSteps = $0["steps"]["step"].all

                // Loop all ingredients in a recipe object
                allIngredients.forEach {
                    guard let name = $0["name"].element?.text else { return }
                    guard let type = $0["type"].element?.text else { return }
                    guard let quantity = $0["quantity"].element?.text else { return }
                    let ingredient = NewRecipe.Ingredient(name: name, type: type, quantity: quantity)
                    ingredients.append(ingredient)
                }

                // Loop all steps in a recipe object
                allSteps.forEach {
                    guard let step = $0.element?.text else { return }
                    steps.append(step)
                }

                let recipe = NewRecipe(title: title, type: type, imageUrl: imageUrl,
                                       ingredients: ingredients, steps: steps)

                // Create a new recipe and save it to core data
                create(a: recipe)

                // Remove data in array to prevent duplicated data
                ingredients.removeAll()
                steps.removeAll()
            }
        }
    }

    func create(a recipe: NewRecipe) {
        DataService.shared.createRecipe(recipe) { success, error in
            guard error == nil && success else {
                print("\(String(describing: error)) at \(#function) in \(#file)")
                return
            }
        }
    }
}
