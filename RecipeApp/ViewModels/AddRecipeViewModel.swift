//
//  AddRecipeViewModel.swift
//  RecipeApp
//
//  Created by Alwyn Yeo on 8/4/21.
//

// MARK: - Class
final class AddRecipeViewModel {

    // MARK: - Public Declarations
    private(set) var recipe: Recipe?
    private(set) var title: String?
    private(set) var type: String?
    private(set) var imageUrl = "https://saltywahine.com/wp-content/uploads/2019/04/Slow-Cooker-Pot-Roast2.jpg"
    private(set) var ingredients: [NewRecipe.Ingredient] = []
    private(set) var steps: [String] = []
    enum DataType: CaseIterable {
        case recipeType
        case ingredients
        case steps
    }

    // MARK: - Public Declarations
    func updateTitle(_ title: String) {
        self.title = title
    }

    func updateType(_ type: String) {
        self.type = type
    }

    func updateImageUrl(_ imageUrl: String) {
        self.imageUrl = imageUrl
    }

    func appendIngredient(_ ingredient: NewRecipe.Ingredient) {
        ingredients.append(ingredient)
    }

    func appendStep(step: String) {
        steps.append(step)
    }

    func move(_ ingredient: NewRecipe.Ingredient, from: Int, to: Int) {
        ingredients.remove(at: from)
        ingredients.insert(ingredient, at: to)
    }

    func removeIngredient(at index: Int) {
        ingredients.remove(at: index)
    }

    func move(_ step: String, from: Int, to: Int) {
        steps.remove(at: from)
        steps.insert(step, at: to)
    }
    
    func removeStep(at index: Int) {
        steps.remove(at: index)
    }
}
