//
//  HomeDetailController.swift
//  RecipeApp
//
//  Created by Alwyn Yeo on 8/3/21.
//

import UIKit

// MARK: - Class
final class HomeDetailController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var header: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet var imageViewTapGestureRecognizer: UITapGestureRecognizer!

    // MARK: - Public Declarations
    var viewModel: HomeDetailViewModel?

    // MARK: - Private Declarations
    private let cellID = "HomeDetailCell"

    // MARK: - Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        makeConfiguration()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.toPickAnImageController {
            if let destinationController = segue.destination as? UINavigationController, let targetController = destinationController.topViewController as? PickAnImageController {
                targetController.delegate = self
            }
        }
    }

    // MARK: - IBActions
    @IBAction
    private func handleReorderButtonItem(_ sender: UIBarButtonItem) {
        tableView.setEditing(!tableView.isEditing, animated: true)
        navigationItem.rightBarButtonItems?[1].title = tableView.isEditing ? "Done" : "Reorder"
        imageView.isUserInteractionEnabled = !tableView.isEditing
    }

    @IBAction
    private func handleEditButtonItem(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Edit Name", message: nil, preferredStyle: .alert)

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)

        alertController.addAction(UIAlertAction(title: "OK", style: .default) { _ in
            guard let textFields = alertController.textFields else { return }
            guard let title = textFields[0].text else { return }

            if !title.isEmpty {
                self.viewModel?.update(for: .title, with: title)
                self.navigationItem.title = self.viewModel?.title
            }
        })

        alertController.addTextField { textField in
            textField.text = self.navigationItem.title
            textField.autocapitalizationType = .sentences
            textField.returnKeyType = .continue
            textField.enablesReturnKeyAutomatically = true
            textField.clearButtonMode = .whileEditing
        }

        alertController.addAction(cancelAction)

        present(alertController, animated: true)
    }

    // MARK: - Private Methods
    private func showEditRecipeTypeAlert(for item: String) {

        let alertController = UIAlertController(title: "Edit Recipe Type", message: "Meat, Grains, Vegetable, Dairy, Fruit", preferredStyle: .alert)

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)

        alertController.addAction(UIAlertAction(title: "OK", style: .default) { _ in
            guard let textFields = alertController.textFields else { return }
            guard let recipeType = textFields[0].text else { return }

            if !recipeType.isEmpty {
                if recipeType == "Meat" || recipeType == "Grains" || recipeType == "Vegetable" || recipeType == "Dairy" || recipeType == "Fruit" {
                    self.viewModel?.update(for: .type, with: recipeType)
                    self.tableView.reloadData()
                } else {
                    Alert.errorAlert(on: self, with: "Only Meat, Grains, Vegetable, Dairy and Fruit are acceptable.")
                }
            }
        })

        alertController.addTextField { textField in
            textField.text = self.viewModel?.type
            textField.autocapitalizationType = .sentences
            textField.returnKeyType = .default
            textField.clearButtonMode = .whileEditing
        }

        alertController.addAction(cancelAction)

        present(alertController, animated: true)
    }

    private func showEditIngredientAlert(for item: Ingredient) {

        let alertController = UIAlertController(title: "Edit Ingredients", message: nil, preferredStyle: .alert)

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)

        alertController.addTextField { textField in
            textField.text = item.name
            textField.autocapitalizationType = .sentences
            textField.returnKeyType = .next
            textField.clearButtonMode = .whileEditing
        }

        alertController.addTextField { textField in
            textField.text = item.type
            textField.autocapitalizationType = .sentences
            textField.returnKeyType = .next
            textField.clearButtonMode = .whileEditing
        }

        alertController.addTextField { textField in
            textField.text = item.quantity
            textField.autocapitalizationType = .sentences
            textField.returnKeyType = .continue
            textField.clearButtonMode = .whileEditing
        }

        alertController.addAction(UIAlertAction(title: "OK", style: .default) { _ in
            guard let textFields = alertController.textFields else { return }
            guard let name = textFields[0].text else { return }
            guard let type = textFields[1].text else { return }
            guard let quantity = textFields[2].text else { return }
            let ingredient = NewRecipe.Ingredient(name: name, type: type, quantity: quantity)
            self.viewModel?.updateIngredient(oldName: item.name!, newData: ingredient)
            self.tableView.reloadData()
        })

        alertController.addAction(cancelAction)

        present(alertController, animated: true)
    }

    private func showEditStepAlert(for item: String) {

        let alertController = UIAlertController(title: "Edit Steps", message: nil, preferredStyle: .alert)

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)

        alertController.addTextField { textField in
            textField.text = item
            textField.autocapitalizationType = .sentences
            textField.returnKeyType = .continue
            textField.clearButtonMode = .whileEditing
        }

        alertController.addAction(UIAlertAction(title: "OK", style: .default) { _ in
            guard let textFields = alertController.textFields else { return }
            guard let step = textFields[0].text else { return }

            if step != item {
                self.viewModel?.updateStep(oldStep: item, newStep: step)
                self.tableView.reloadData()
            }
        })

        alertController.addAction(cancelAction)

        present(alertController, animated: true)
    }

    // MARK: - Objc Methods
    @objc
    private func handleAddButton(_ sender: UIButton) {
        let alertController = UIAlertController(title: sender.tag == .zero ? "Add New Ingredient" : "Add New Step", message: nil, preferredStyle: .alert)

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)

        if sender.tag == .zero {
            alertController.addTextField { textField in
                textField.placeholder = "Name"
                textField.autocapitalizationType = .sentences
                textField.returnKeyType = .next
                textField.enablesReturnKeyAutomatically = true
                textField.clearButtonMode = .whileEditing
            }

            alertController.addTextField { textField in
                textField.placeholder = "Type"
                textField.autocapitalizationType = .sentences
                textField.returnKeyType = .next
                textField.enablesReturnKeyAutomatically = true
                textField.clearButtonMode = .whileEditing
            }

            alertController.addTextField { textField in
                textField.placeholder = "Quantity"
                textField.autocapitalizationType = .sentences
                textField.returnKeyType = .continue
                textField.enablesReturnKeyAutomatically = true
                textField.clearButtonMode = .whileEditing
            }
            
            alertController.addAction(UIAlertAction(title: "OK", style: .default) { _ in
                guard let textFields = alertController.textFields else { return }
                guard let name = textFields[0].text else { return }
                guard let type = textFields[1].text else { return }
                guard let quantity = textFields[2].text else { return }

                if name.isEmpty || type.isEmpty || quantity.isEmpty {
                    Alert.errorAlert(on: self, with: "All fields are mandatory.")
                } else {
                    let ingredient = NewRecipe.Ingredient(name: name, type: type, quantity: quantity)
                    self.viewModel?.appendIngredient(ingredient)
                    self.tableView.reloadData()
                }
            })
        } else {
            alertController.addTextField { textField in
                textField.placeholder = "New Step"
                textField.autocapitalizationType = .sentences
                textField.returnKeyType = .continue
                textField.enablesReturnKeyAutomatically = true
                textField.clearButtonMode = .whileEditing
            }

            alertController.addAction(UIAlertAction(title: "OK", style: .default) { _ in
                guard let textFields = alertController.textFields else { return }
                guard let step = textFields[0].text else { return }
                if step.isEmpty {
                    Alert.errorAlert(on: self, with: "This field is mandatory.")
                } else {
                    self.viewModel?.appendStep(step: step)
                    self.tableView.reloadData()
                }
            })
        }

        alertController.addAction(cancelAction)

        present(alertController, animated: true)
    }
}

// MARK: - PickAnImageControllerDelegate
extension HomeDetailController: PickAnImageControllerDelegate {
    func receive(imageUrl: String) {
        viewModel?.update(for: .imageUrl, with: imageUrl)
        imageView.sd_setImage(with: URL(string: imageUrl))
    }
}

// MARK: - UITableViewDelegate
extension HomeDetailController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = indexPath.section
        let item = indexPath.row

        if section == .zero {
            guard let recipeType = viewModel?.type else { return }
            showEditRecipeTypeAlert(for: recipeType)
        } else if section == 1 {
            guard let ingredient = viewModel?.ingredients[item] else { return }
            showEditIngredientAlert(for: ingredient)
        } else if section == 2 {
            guard let step = viewModel?.steps[item] else { return }
            showEditStepAlert(for: step)
        }
    }
}

// MARK: - UITableViewDataSource
extension HomeDetailController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let section = indexPath.section
        let item = indexPath.row
        if editingStyle == .delete {
            if section == 1 {
                viewModel?.removeIngredient(at: item)
                tableView.deleteRows(at: [indexPath], with: .fade)
            } else {
                viewModel?.removeStep(at: item)
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        }
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        indexPath.section != .zero
    }

    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        indexPath.section == 2
    }

    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let section = sourceIndexPath.section

        if section == 2 {
            guard let step = viewModel?.steps[sourceIndexPath.row] else { return }
            viewModel?.move(step, from: sourceIndexPath.row, to: destinationIndexPath.row)
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        let titleLabel = UILabel()
        let addButton = UIButton(type: .system)

        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textColor = .systemGray
        titleLabel.font = .systemFont(ofSize: 14, weight: .medium)

        addButton.translatesAutoresizingMaskIntoConstraints = false
        addButton.addTarget(self, action: #selector(handleAddButton(_:)), for: .touchUpInside)

        view.addSubview(titleLabel)
        view.addSubview(addButton)

        NSLayoutConstraint.activate([
            titleLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            addButton.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            addButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16)
        ])

        if section == .zero {
            return nil
        } else if section == 1 {
            titleLabel.text = "INGREDIENTS"
            addButton.tag = .zero
            addButton.setTitle("Add", for: .normal)
        } else {
            titleLabel.text = "STEPS"
            addButton.tag = 1
            addButton.setTitle("Add", for: .normal)
        }

        return view
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        3
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == .zero {
            return 1
        } else if section == 1 {
            return viewModel?.ingredients.count ?? .zero
        } else {
            return viewModel?.steps.count ?? .zero
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        let section = indexPath.section

        cell.textLabel?.numberOfLines = .zero

        if section == .zero {
            cell.textLabel?.text = viewModel?.type
        } else if section == 1 {
            let ingredient = viewModel?.ingredients[indexPath.row]
            cell.textLabel?.text = """
            Name: \(ingredient?.name ?? "")
            Type: \(ingredient?.type ?? "")
            Quantity: \(ingredient?.quantity ?? "")
            """
        } else {
            cell.textLabel?.text = viewModel?.steps[indexPath.row]
        }

        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        section == .zero ? "Recipe Type" : nil
    }
}

// MARK: - Configuration
private extension HomeDetailController {
    private func makeConfiguration() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        configureData()
    }

    private func configureData() {
        let imageUrl = URL(string: viewModel?.imageUrl ?? "")

        navigationItem.title = viewModel?.title

        imageView.sd_setImage(with: imageUrl)
    }
}
