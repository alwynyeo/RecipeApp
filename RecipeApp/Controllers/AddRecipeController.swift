//
//  AddRecipeController.swift
//  RecipeApp
//
//  Created by Alwyn Yeo on 8/4/21.
//

import UIKit

// MARK: - Protocol
protocol AddRecipeControllerDelegate: AnyObject {
    func save(for recipe: NewRecipe)
}

// MARK: - Class
final class AddRecipeController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!

    // MARK: - Public Declarations
    weak var delegate: AddRecipeControllerDelegate?

    var viewModel: AddRecipeViewModel?

    // MARK: - Private Declarations
    private let cellID = "AddRecipeCell"
    
    // MARK: - Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        makeConfiguration()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.toPickAnImageController {
            if let destinationController = segue.destination as? UINavigationController, let targetController = destinationController.topViewController as? PickAnImageController {
                targetController.delegate = self
            }
        }
    }

    // MARK: - IBActions
    @IBAction
    private func handleEditButtonItem(_ sender: UIBarButtonItem) {
        tableView.setEditing(!tableView.isEditing, animated: true)
    }

    @IBAction
    private func boundsTapGestureRecognizer(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }

    @IBAction
    private func nameTextFieldDidChange(_ sender: UITextField) {
        guard let title = sender.text else { return }
        viewModel?.updateTitle(title)
        isValidated(title: viewModel?.title, type: viewModel?.type, ingredients: viewModel?.ingredients, steps: viewModel?.steps)
    }

    @IBAction
    private func handleAddIngredientsButton(_ sender: UIButton) {
        if view.isFocused {
            view.endEditing(true)
        }
        addRecipeAlert(for: .ingredients)
    }

    @IBAction
    private func handleAddStepsButton(_ sender: UIButton) {
        if view.isFocused {
            view.endEditing(true)
        }
        addRecipeAlert(for: .steps)
    }

    @IBAction
    private func handleSaveButton(_ sender: UIButton) {
        guard let title = viewModel?.title else { return }
        guard let type = viewModel?.type else { return }
        guard let imageUrl = viewModel?.imageUrl else { return }
        guard let ingredients = viewModel?.ingredients else { return }
        guard let steps = viewModel?.steps else { return }

        let recipe = NewRecipe(title: title, type: type, imageUrl: imageUrl, ingredients: ingredients, steps: steps)

        delegate?.save(for: recipe)

        dismiss(animated: true)
    }

    // MARK: - Private Methods
    private func setEditButtonItemState() {
        guard let ingredients = viewModel?.ingredients else { return }
        guard let steps = viewModel?.steps else { return }
        navigationItem.leftBarButtonItem?.isEnabled = !ingredients.isEmpty || !steps.isEmpty
    }

    private func addRecipeAlert(for type: AddRecipeViewModel.DataType) {
        let title = type == .ingredients ? "Add Ingredients" : "Add Steps"
        let alertController = UIAlertController(title: title, message: nil, preferredStyle: .alert)

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)

        switch type {
            case .ingredients:
                alertController.addAction(UIAlertAction(title: "OK", style: .default) { _ in
                    guard let textFields = alertController.textFields else { return }
                    guard let name = textFields[0].text else { return }
                    guard let type = textFields[1].text else { return }
                    guard let quantity = textFields[2].text else { return }

                    if name.isEmpty || type.isEmpty || quantity.isEmpty {
                        Alert.errorAlert(on: self, with: "All fields are mandatory.")
                    } else {
                        let ingredient = NewRecipe.Ingredient(name: name, type: type, quantity: quantity)
                        self.viewModel?.appendIngredient(ingredient)
                        self.tableView.reloadData()
                        self.setEditButtonItemState()
                        self.isValidated(title: self.viewModel?.title, type: self.viewModel?.type, ingredients: self.viewModel?.ingredients, steps: self.viewModel?.steps)
                    }
                })

                alertController.addTextField { textField in
                    textField.placeholder = "Name"
                    textField.autocapitalizationType = .sentences
                    textField.returnKeyType = .next
                    textField.clearButtonMode = .whileEditing
                    textField.enablesReturnKeyAutomatically = true
                }

                alertController.addTextField { textField in
                    textField.placeholder = "Type"
                    textField.autocapitalizationType = .sentences
                    textField.returnKeyType = .next
                    textField.clearButtonMode = .whileEditing
                    textField.enablesReturnKeyAutomatically = true
                }

                alertController.addTextField { textField in
                    textField.placeholder = "Quantity"
                    textField.autocapitalizationType = .sentences
                    textField.returnKeyType = .continue
                    textField.enablesReturnKeyAutomatically = true
                    textField.clearButtonMode = .whileEditing
                    textField.enablesReturnKeyAutomatically = true
                }
            case .steps:
                alertController.addAction(UIAlertAction(title: "OK", style: .default) { _ in
                    guard let textFields = alertController.textFields else { return }
                    guard let step = textFields[0].text else { return }
                    if step.isEmpty {
                        Alert.errorAlert(on: self, with: "This field is mandatory.")
                    } else {
                        self.viewModel?.appendStep(step: step)
                        self.tableView.reloadData()
                        self.setEditButtonItemState()
                        self.isValidated(title: self.viewModel?.title, type: self.viewModel?.type, ingredients: self.viewModel?.ingredients, steps: self.viewModel?.steps)
                    }
                })

                alertController.addTextField { textField in
                    textField.placeholder = "Step"
                    textField.autocapitalizationType = .sentences
                    textField.returnKeyType = .continue
                    textField.clearButtonMode = .whileEditing
                    textField.enablesReturnKeyAutomatically = true
                }
            default: return
        }

        alertController.addAction(cancelAction)

        present(alertController, animated: true)
    }

    private func isValidated(title: String?, type: String?, ingredients: [NewRecipe.Ingredient]?, steps: [String]?) {
        guard let title = title, let type = type, let ingredients = ingredients, let steps = steps else { return }
        navigationItem.rightBarButtonItem?.isEnabled = !title.isEmpty && !type.isEmpty && !ingredients.isEmpty && !steps.isEmpty
    }

    // MARK: - Deinit
    deinit {
        print("Deinit AddRecipeController")
    }
    
}

// MARK: - PickAnImageControllerDelegate
extension AddRecipeController: PickAnImageControllerDelegate {
    func receive(imageUrl: String) {
        self.viewModel?.updateImageUrl(imageUrl)
        imageView.sd_setImage(with: URL(string: imageUrl))
    }
}

// MARK: - UITextFieldDelegate
extension AddRecipeController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

// MARK: - UITableViewDelegate
extension AddRecipeController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = indexPath.section
        let cell = tableView.cellForRow(at: indexPath)
        if section == .zero {
            viewModel?.updateType(Constants.recipeType[indexPath.row])
            cell?.accessoryType = .checkmark
            isValidated(title: viewModel?.title, type: viewModel?.type, ingredients: viewModel?.ingredients, steps: viewModel?.steps)
        }
        cell?.selectionStyle = .none
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .none
    }

    func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        if sourceIndexPath.section != proposedDestinationIndexPath.section {
            var row = 0
            if sourceIndexPath.section < proposedDestinationIndexPath.section {
                row = self.tableView(tableView, numberOfRowsInSection: sourceIndexPath.section) - 1
            }
            return IndexPath(row: row, section: sourceIndexPath.section)
        }
        return proposedDestinationIndexPath
    }
}

// MARK: - UITableViewDataSource
extension AddRecipeController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == .zero {
            return "Recipe Type"
        } else if section == 1 {
            return "Ingredients"
        } else {
            return "Steps"
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        AddRecipeViewModel.DataType.allCases.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == .zero {
            return Constants.recipeType.count
        } else if section == 1 {
            return viewModel?.ingredients.count ?? .zero
        } else {
            return viewModel?.steps.count ?? .zero
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        let section = indexPath.section

        cell.textLabel?.numberOfLines = .zero

        if section == .zero {
            cell.textLabel?.text = Constants.recipeType[indexPath.row]
        } else if section == 1 {
            let item = viewModel?.ingredients[indexPath.row]
            cell.textLabel?.text = """
            Name: \(item?.name ?? "")
            Type: \(item?.type ?? "")
            Quantity: \(item?.quantity ?? "")
            """
        } else {
            cell.textLabel?.text = viewModel?.steps[indexPath.row]
        }

        return cell
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        indexPath.section != .zero
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let section = indexPath.section
        let item = indexPath.row
        if editingStyle == .delete {
            if section == 1 {
                viewModel?.removeIngredient(at: item)
                tableView.deleteRows(at: [indexPath], with: .fade)

            } else {
                viewModel?.removeStep(at: item)
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            setEditButtonItemState()
            isValidated(title: viewModel?.title, type: viewModel?.type, ingredients: viewModel?.ingredients, steps: viewModel?.steps)
        }
    }

    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        indexPath.section != .zero
    }

    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let section = sourceIndexPath.section

        if section == 1 {
            guard let ingredient = viewModel?.ingredients[sourceIndexPath.row] else { return }
            viewModel?.move(ingredient, from: sourceIndexPath.row, to: destinationIndexPath.row)
        } else {
            guard let step = viewModel?.steps[sourceIndexPath.row] else { return }
            viewModel?.move(step, from: sourceIndexPath.row, to: destinationIndexPath.row)
        }
    }
}

// MARK: - Configuration
private extension AddRecipeController {
    private func makeConfiguration() {
        view.accessibilityIdentifier = "AddRecipeController"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        nameTextField.delegate = self
        navigationItem.rightBarButtonItem?.isEnabled = false
        navigationItem.leftBarButtonItem?.isEnabled = false
    }
}
