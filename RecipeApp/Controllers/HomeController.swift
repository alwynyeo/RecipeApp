//
//  HomeController.swift
//  RecipeApp
//
//  Created by Alwyn Yeo on 8/3/21.
//

import UIKit

// MARK: - Class
final class HomeController: UICollectionViewController {

    // MARK: - Private Declarations
    private lazy var dataSource = makeDataSource()
    private let viewModel = HomeViewModel()
    private let cellConfigurator = HomeCellConfigurator()
    private var filterTextField: UITextField!
    private lazy var interaction = UIContextMenuInteraction(delegate: self)

    private lazy var filterPickerView: UIPickerView = {
        let pickerview = UIPickerView()
        pickerview.delegate = self
        pickerview.dataSource = self
        return pickerview
    }()

    private lazy var filterPickerViewToolBar: FilterPickerViewToolBar = {
        let frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 49)
        let toolBar = FilterPickerViewToolBar(frame: frame)
        toolBar.cancelBarButtonItem.action = #selector(handleToolBarButtonItem(_:))
        toolBar.doneBarButtonItem.action = #selector(handleToolBarButtonItem(_:))
        return toolBar
    }()

    // Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        makeConfiguration()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collectionView.reloadData()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.toAddRecipeController {
            if let destinationController = segue.destination as? UINavigationController, let targetController = destinationController.topViewController as? AddRecipeController {
                targetController.delegate = self
                targetController.viewModel = AddRecipeViewModel()
            }
        }
    }

    // MARK: - Private Methods
    private func deleteAction(indexPath: IndexPath) -> UIAction {
        let action = UIAction(title: "Delete", image: UIImage(systemName: "trash"), attributes: .destructive) { _ in
            var snapshot = self.dataSource.snapshot()
            guard let recipe = self.dataSource.itemIdentifier(for: indexPath) else { return }
            self.viewModel.deleteRecipe(recipe, at: indexPath.row)
            snapshot.deleteItems([recipe])
            self.dataSource.apply(snapshot)
        }
        return action
    }

    private func createContextMenu(indexPath: IndexPath) -> UIMenu {
        let menu = UIMenu(children: [
            deleteAction(indexPath: indexPath)
        ])
        return menu
    }

    // MARK: - Objc Methods
    @objc
    private func handleToolBarButtonItem(_ sender: UIBarButtonItem) {
        let title = sender.title

        switch title {
            case "Done":
                filterPickerView.selectRow(.zero, inComponent: .zero, animated: false)
                    viewModel.selectedFilterType = "All"
                view.endEditing(true)
            case "Cancel":
                view.endEditing(true)
            default: return
        }

        filterTextField.text = viewModel.selectedFilterType
        viewModel.filterRecipes()
        applySnapshot()
    }
}

// MARK: - AddRecipeControllerDelegate
extension HomeController: AddRecipeControllerDelegate {
    func save(for recipe: NewRecipe) {
        viewModel.create(a: recipe)
        viewModel.fetchAllRecipes()
        viewModel.filterRecipes()
        applySnapshot()
    }
}

// MARK: - UIPickerViewDelegate
extension HomeController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        Constants.recipeType[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let type = Constants.recipeType[row]
        viewModel.selectedFilterType = type
        filterTextField.text = viewModel.selectedFilterType
        viewModel.filterRecipes()
        applySnapshot()
    }
}

// MARK: - UIPickerViewDataSource
extension HomeController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        Constants.recipeType.count
    }
}

// MARK: - UIContextMenuInteractionDelegate
extension HomeController: UIContextMenuInteractionDelegate {
    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
        nil
    }
}

// MARK: - UICollectionView Methods
extension HomeController {
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let selectedRecipe = dataSource.itemIdentifier(for: indexPath) else { return }
        guard let detailController = storyboard?.instantiateViewController(
                identifier: Constants.homeDetailController) as? HomeDetailController else {
            return
        }
        detailController.viewModel = HomeDetailViewModel(recipe: selectedRecipe)
        navigationController?.pushViewController(detailController, animated: true)
    }

    override func collectionView(_ collectionView: UICollectionView, contextMenuConfigurationForItemAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { _ -> UIMenu? in
            self.createContextMenu(indexPath: indexPath)
        }
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension HomeController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: (collectionView.frame.width - 48) / 2, height: 200)
    }
}

// MARK: - Diffable Data Source
private extension HomeController {
    private typealias Cell = HomeCell
    private typealias DataSource = UICollectionViewDiffableDataSource<Section, Recipe>
    private typealias Snapshot = NSDiffableDataSourceSnapshot<Section, Recipe>

    private func makeDataSource() -> DataSource {
        let dataSource = DataSource(collectionView: collectionView) { collectionView, indexPath, recipe in
            guard let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: Cell.cellID,
                    for: indexPath) as? Cell else {
                return UICollectionViewCell()
            }
            self.cellConfigurator.configure(cell, forDisplaying: recipe)
            return cell
        }
        dataSource.supplementaryViewProvider = ({ collectionView, kind, indexPath in
            guard let header = collectionView.dequeueReusableSupplementaryView(ofKind: Constants.headerKind, withReuseIdentifier: HomeHeader.cellID, for: indexPath) as? HomeHeader else {
                return UICollectionReusableView()
            }
            header.filterTextField.inputView = self.filterPickerView
            header.filterTextField.inputAccessoryView = self.filterPickerViewToolBar
            header.filterTextField.text = self.viewModel.selectedFilterType
            self.filterTextField = header.filterTextField
            return header
        })
        return dataSource
    }

    private func applySnapshot() {
        var snapshot = Snapshot()
        snapshot.appendSections(Section.allCases)
        snapshot.appendItems(viewModel.filteredRecipes, toSection: .main)
        dataSource.apply(snapshot)
    }

    private enum Section: CaseIterable {
        case main
    }
}

// MARK: - Configuration
private extension HomeController {
    private func makeConfiguration() {
        viewModel.fetchAllRecipes()
        applySnapshot()
    }
}
