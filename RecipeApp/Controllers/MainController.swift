//
//  MainController.swift
//  RecipeApp
//
//  Created by Alwyn Yeo on 8/2/21.
//

import UIKit

// MARK: - Class
final class MainController: UITabBarController {

    // MARK: - Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
