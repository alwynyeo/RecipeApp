//
//  PickAnImageController.swift
//  RecipeApp
//
//  Created by Alwyn Yeo on 8/4/21.
//

import UIKit
import SDWebImage

// MARK: - Protocol
protocol PickAnImageControllerDelegate: AnyObject {
    func receive(imageUrl: String)
}

// MARK: - Class
final class PickAnImageController: UICollectionViewController {

    // MARK: - Public Declarations
    weak var delegate: PickAnImageControllerDelegate?

    // MARK: - Private Declarations
    private let imageUrls = [
        "https://i.ytimg.com/vi/SQoaszSmk80/maxresdefault.jpg",
        "https://www.heartfoundation.org.nz/media/images/all-shared-sections/blogs/how-to-eat-more-whole-grains_737_373_c1.jpg",
        "https://images.squarespace-cdn.com/content/v1/57a100f7e4fcb592ee30586c/1515382496530-IE1DRM7ZJBEY9SKCYPN9/Depositphotos_162692674_l-2015.jpg?format=1000w",
        "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/pasta-salad-horizontal-jpg-1522265695.jpg",
        "https://www.seriouseats.com/thmb/1OdmFf9djrsvcLxbHs4JKdwj2lo=/1500x1125/filters:fill(auto,1)/__opt__aboutcom__coeus__resources__content_migration__serious_eats__seriouseats.com__2019__06__20190614-yogurt-vicky-wasik-8-1b8381eea1b44c17ac31879c11e6c624.jpg"
    ]

    // MARK: - Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

// MARK: - UICollectionView Methods
extension PickAnImageController {

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        imageUrls.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PickAnImageCell.cellID, for: indexPath) as? PickAnImageCell else {
            return UICollectionViewCell()
        }
        let imageUrl = URL(string: imageUrls[indexPath.item])
        cell.imageView.sd_setImage(with: imageUrl) { _, _, _, _ in
            cell.activityIndicatorView.stopAnimating()
        }
        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let imageUrl = imageUrls[indexPath.item]
        delegate?.receive(imageUrl: imageUrl)
        dismiss(animated: true)
    }
}
