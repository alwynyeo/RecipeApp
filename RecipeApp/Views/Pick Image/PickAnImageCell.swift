//
//  PickAnImageCell.swift
//  RecipeApp
//
//  Created by Alwyn Yeo on 8/4/21.
//

import UIKit

// MARK: - Class
final class PickAnImageCell: UICollectionViewCell {

    // MARK: - Public Declarations
    static let cellID = "PickAnImageCell"

    // MARK: - IBOutlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
}
