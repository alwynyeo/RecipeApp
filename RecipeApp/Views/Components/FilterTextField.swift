//
//  FilterTextField.swift
//  RecipeApp
//
//  Created by Alwyn Yeo on 8/4/21.
//

import UIKit

// MARK: - Class
final class FilterTextField: UITextField {

    // MARK: - Override Methods
    override func caretRect(for position: UITextPosition) -> CGRect {
        return .zero
    }

    override func selectionRects(for range: UITextRange) -> [UITextSelectionRect] {
        return []
    }

    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        let editActions = UIResponderStandardEditActions.self
        if action == #selector(editActions.copy(_:)) || action == #selector(editActions.selectAll(_:)) || action == #selector(editActions.paste(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
}
