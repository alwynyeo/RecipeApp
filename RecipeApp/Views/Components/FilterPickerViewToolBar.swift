//
//  FilterPickerViewToolBar.swift
//  RecipeApp
//
//  Created by Alwyn Yeo on 8/4/21.
//

import UIKit

// MARK: - Class
final class FilterPickerViewToolBar: UIToolbar {

    // MARK: - Public Declarations
    let cancelBarButtonItem: UIBarButtonItem = {
        let barButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: nil)
        return barButtonItem
    }()

    let doneBarButtonItem: UIBarButtonItem = {
        let barButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: nil)
        return barButtonItem
    }()

    // MARK: - Fileprivate Declarations
    fileprivate let flexibleBarButtonItem = UIBarButtonItem(
        barButtonSystemItem: .flexibleSpace,
        target: nil,
        action: nil
    )

    // MARK: - Override Methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    // MARK: - Private Methods
    private func setupView() {
        sizeToFit()
        setItems([
            cancelBarButtonItem,
            flexibleBarButtonItem,
            doneBarButtonItem,
        ], animated: true)
        translatesAutoresizingMaskIntoConstraints = false
    }

    // MARK: - Required Init
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
