//
//  HomeHeader.swift
//  RecipeApp
//
//  Created by Alwyn Yeo on 8/4/21.
//

import UIKit

// MARK: - Class
final class HomeHeader: UICollectionReusableView {

    // MARK: - Public Declarations
    static let cellID = "HomeHeader"

    // MARK: - IBOutlets
    @IBOutlet weak var filterTextField: UITextField!
}
