//
//  HomeCellConfigurator.swift
//  RecipeApp
//
//  Created by Alwyn Yeo on 8/3/21.
//

import UIKit
import SDWebImage

// MARK: - Class
final class HomeCellConfigurator {

    // MARK: - Public Declarations
    func configure(_ cell: HomeCell, forDisplaying recipe: Recipe) {
        let imageUrl = URL(string: recipe.imageUrl)
        cell.titleLabel.text = recipe.title
        cell.recipeTypeLabel.text = recipe.type
        cell.imageView.sd_setImage(with: imageUrl) { _, _, _, _ in
            cell.activityIndicatorView.stopAnimating()
        }
    }
}
