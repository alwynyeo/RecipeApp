//
//  HomeCell.swift
//  RecipeApp
//
//  Created by Alwyn Yeo on 8/3/21.
//

import UIKit

// MARK: - Class
final class HomeCell: UICollectionViewCell {

    // MARK: - Public Declarations
    static let cellID = "HomeCell"

    // MARK: - IBOutlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var recipeTypeLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
}

