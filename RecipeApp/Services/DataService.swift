//
//  DataService.swift
//  RecipeApp
//
//  Created by Alwyn Yeo on 8/4/21.
//

import UIKit
import CoreData

// MARK: - Class
final class DataService {

    // MARK: - Public Declarations
    static let shared = DataService()

    // MARK: - Private Declarations
    private lazy var context = ( UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    // MARK: - Public Methods
    func fetchAllRecipes(completion: @escaping RecipesHandler) {
        do {
            guard let recipes = try context.fetch(Recipe.fetchRequest()) as? [Recipe] else { return }
            completion(.success(recipes))
        } catch {
            completion(.failure(error))
        }
    }

    func createRecipe(_ data: NewRecipe, completion: @escaping BooleanHandler) {
        let recipe = Recipe(context: context)

        recipe.title = data.title
        recipe.type = data.type
        recipe.imageUrl = data.imageUrl
        recipe.steps = data.steps

        data.ingredients.forEach {
            let ingredient = Ingredient(context: context)
            ingredient.name = $0.name
            ingredient.type = $0.type
            ingredient.quantity = $0.quantity
            recipe.addToIngredient(ingredient)
        }

        // Save data
        do {
            try context.save()
            completion(true, nil)
        } catch {
            completion(false, error)
        }
    }

    func createIngredient(for recipe: Recipe, _ data: NewRecipe.Ingredient, completion: @escaping IngredientHandler) {
        let ingredient = Ingredient(context: context)
        ingredient.name = data.name
        ingredient.type = data.type
        ingredient.quantity = data.quantity
        recipe.addToIngredient(ingredient)

        do {
            try context.save()
            completion(.success(ingredient))
        } catch {
            completion(.failure(error))
        }
    }

    func updateRecipe(completion: @escaping BooleanHandler) {
        // Update data
        do {
            try context.save()
            completion(true, nil)
        } catch {
            completion(false, error)
        }
    }

    func updateIngredient(from oldName: String, to newData: NewRecipe.Ingredient, completion: @escaping BooleanHandler) {
        var ingredients = [Ingredient]()
        let fetchRequest: NSFetchRequest<Ingredient> = Ingredient.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "name = %@", oldName)

        do {
            ingredients = try context.fetch(fetchRequest) as [Ingredient]
        } catch {
            completion(false, error)
        }

        guard let currentIngredient = ingredients.first else {
            completion(false, nil)
            return
        }

        currentIngredient.name = newData.name
        currentIngredient.type = newData.type
        currentIngredient.quantity = newData.quantity

        do {
            try context.save()
        } catch {
            completion(false, error)
        }

        completion(true, nil)
    }

    func deleteRecipe(at recipe: Recipe, completion: @escaping BooleanHandler) {
        // Delete recipe
        context.delete(recipe)

        // Save
        do {
            try context.save()
            completion(true, nil)
        } catch {
            completion(false, error)
        }
    }

    func deleteIngredient(_ ingredient: Ingredient, completion: @escaping BooleanHandler) {
        // Delete ingredient
        context.delete(ingredient)

        // Save
        do {
            try context.save()
            completion(true, nil)
        } catch {
            completion(false, error)
        }
    }

    // Check if core data has data objects
    func isEmpty() -> Bool {
        var result = false
        do {
            let count = try context.count(for: Recipe.fetchRequest())
            result = count == .zero ? true : false
        } catch {
            result = true
        }
        return result
    }
}
