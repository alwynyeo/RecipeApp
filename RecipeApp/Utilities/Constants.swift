//
//  Constants.swift
//  RecipeApp
//
//  Created by Alwyn Yeo on 8/3/21.
//

import UIKit

// MARK: - Public Typealiases
typealias BooleanHandler = (_ success: Bool, _ error: Error?) -> Void
typealias RecipesHandler = (Result<[Recipe], Error>) -> Void
typealias IngredientHandler = (Result<Ingredient, Error>) -> Void

// MARK: - Struct
struct Constants {

    // MARK: - Public Declarations
    static let headerKind = UICollectionView.elementKindSectionHeader
    static let homeDetailController = "homeDetailController"
    static let toAddRecipeController = "toAddRecipeController"
    static let toPickAnImageController = "toPickAnImageController"
    static let recipeType = [
        "Meat",
        "Grains",
        "Vegetable",
        "Dairy",
        "Fruit"
    ]
}
