//
//  UIViewExtension.swift
//  RecipeApp
//
//  Created by Alwyn Yeo on 8/3/21.
//

import UIKit

// MARK: - UIView Extension
extension UIView {

    // MARK: - IBInspectable
    @IBInspectable
    var cornerRadius: CGFloat {
        get { layer.cornerRadius }
        set { layer.cornerRadius = newValue }
    }
}
