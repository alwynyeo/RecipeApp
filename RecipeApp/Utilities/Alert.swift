//
//  Alert.swift
//  RecipeApp
//
//  Created by Alwyn Yeo on 8/4/21.
//

import UIKit

// MARK: - Struct
struct Alert {

    // MARK: - Public Methods
    static func errorAlert(on viewController: UIViewController?, with title: String?) {
        let alertController = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
        alertController.addAction(okAction)
        DispatchQueue.main.async {
            viewController?.present(alertController, animated: true, completion: nil)
        }
    }
}
